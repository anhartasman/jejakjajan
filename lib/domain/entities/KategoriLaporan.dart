import 'dart:convert';

import 'package:flutter/foundation.dart';

class KategoriLaporan {
  final int id;
  final String nama;
  final String kode;
  final String icon_path;
  final String icon_hex_color;
  const KategoriLaporan({
    required this.id,
    required this.nama,
    required this.kode,
    required this.icon_path,
    required this.icon_hex_color,
  });

  KategoriLaporan copyWith({
    int? id,
    String? nama,
    String? kode,
    String? icon_path,
    String? icon_hex_color,
  }) {
    return KategoriLaporan(
      id: id ?? this.id,
      nama: nama ?? this.nama,
      kode: kode ?? this.kode,
      icon_path: icon_path ?? this.icon_path,
      icon_hex_color: icon_hex_color ?? this.icon_hex_color,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nama': nama,
      'kode': kode,
      'icon_path': icon_path,
      'icon_hex_color': icon_hex_color,
    };
  }

  factory KategoriLaporan.fromMap(Map<String, dynamic> map) {
    return KategoriLaporan(
      id: map['id'],
      nama: map['nama'],
      kode: map['kode'],
      icon_path: map['icon_path'],
      icon_hex_color: map['icon_hex_color'],
    );
  }

  String toJson() => json.encode(toMap());

  factory KategoriLaporan.fromJson(String source) =>
      KategoriLaporan.fromMap(json.decode(source));

  @override
  String toString() {
    return 'KategoriLaporan(id: $id, nama: $nama, kode: $kode, icon_path: $icon_path, icon_hex_color: $icon_hex_color)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is KategoriLaporan &&
        other.id == id &&
        other.nama == nama &&
        other.kode == kode &&
        other.icon_path == icon_path &&
        other.icon_hex_color == icon_hex_color;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        nama.hashCode ^
        kode.hashCode ^
        icon_path.hashCode ^
        icon_hex_color.hashCode;
  }
}
