import 'dart:convert';

import 'package:flutter/foundation.dart';

class FilterLaporan {
  int dateFrom;
  int dateTo;
  bool all;
  FilterLaporan({
    required this.dateFrom,
    required this.dateTo,
    this.all = false,
  });

  FilterLaporan copyWith({
    int? dateFrom,
    int? dateTo,
    bool? all,
  }) {
    return FilterLaporan(
      dateFrom: dateFrom ?? this.dateFrom,
      dateTo: dateTo ?? this.dateTo,
      all: all ?? this.all,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'dateFrom': dateFrom,
      'dateTo': dateTo,
      'all': all,
    };
  }

  factory FilterLaporan.fromMap(Map<String, dynamic> map) {
    return FilterLaporan(
      dateFrom: map['dateFrom'],
      dateTo: map['dateTo'],
      all: map['all'],
    );
  }

  String toJson() => json.encode(toMap());

  factory FilterLaporan.fromJson(String source) =>
      FilterLaporan.fromMap(json.decode(source));

  @override
  String toString() =>
      'FilterLaporan(dateFrom: $dateFrom, dateTo: $dateTo, all: $all)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is FilterLaporan &&
        other.dateFrom == dateFrom &&
        other.dateTo == dateTo &&
        other.all == all;
  }

  @override
  int get hashCode => dateFrom.hashCode ^ dateTo.hashCode ^ all.hashCode;
}
