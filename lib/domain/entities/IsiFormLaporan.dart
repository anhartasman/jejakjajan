import 'dart:convert';

import 'package:flutter/foundation.dart';

class IsiFormLaporan {
  int jumlah;
  int tanggal;
  int kategori;
  String nama;
  IsiFormLaporan({
    required this.jumlah,
    required this.tanggal,
    required this.kategori,
    required this.nama,
  });

  IsiFormLaporan copyWith({
    int? jumlah,
    int? tanggal,
    int? kategori,
    String? nama,
  }) {
    return IsiFormLaporan(
      jumlah: jumlah ?? this.jumlah,
      tanggal: tanggal ?? this.tanggal,
      kategori: kategori ?? this.kategori,
      nama: nama ?? this.nama,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jumlah': jumlah,
      'tanggal': tanggal,
      'kategori': kategori,
      'nama': nama,
    };
  }

  factory IsiFormLaporan.fromMap(Map<String, dynamic> map) {
    return IsiFormLaporan(
      jumlah: map['jumlah'],
      tanggal: map['tanggal'],
      kategori: map['kategori'],
      nama: map['nama'],
    );
  }

  String toJson() => json.encode(toMap());

  factory IsiFormLaporan.fromJson(String source) =>
      IsiFormLaporan.fromMap(json.decode(source));

  @override
  String toString() {
    return 'IsiFormLaporan(jumlah: $jumlah, tanggal: $tanggal, kategori: $kategori, nama: $nama)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is IsiFormLaporan &&
        other.jumlah == jumlah &&
        other.tanggal == tanggal &&
        other.kategori == kategori &&
        other.nama == nama;
  }

  @override
  int get hashCode {
    return jumlah.hashCode ^
        tanggal.hashCode ^
        kategori.hashCode ^
        nama.hashCode;
  }
}
