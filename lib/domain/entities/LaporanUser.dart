import 'dart:convert';

import 'package:flutter/foundation.dart';

class LaporanUser {
  String id;
  int jumlah;
  int tanggal;
  int kategori;
  String nama;
  String userId;
  LaporanUser({
    required this.id,
    required this.jumlah,
    required this.tanggal,
    required this.kategori,
    required this.nama,
    required this.userId,
  });

  LaporanUser copyWith({
    String? id,
    int? jumlah,
    int? tanggal,
    int? kategori,
    String? nama,
    String? userId,
  }) {
    return LaporanUser(
      id: id ?? this.id,
      jumlah: jumlah ?? this.jumlah,
      tanggal: tanggal ?? this.tanggal,
      kategori: kategori ?? this.kategori,
      nama: nama ?? this.nama,
      userId: userId ?? this.userId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'jumlah': jumlah,
      'tanggal': tanggal,
      'kategori': kategori,
      'nama': nama,
      'userId': userId,
    };
  }

  factory LaporanUser.fromMap(Map<String, dynamic> map) {
    return LaporanUser(
      id: map['id'],
      jumlah: map['jumlah'],
      tanggal: map['tanggal'],
      kategori: map['kategori'],
      nama: map['nama'],
      userId: map['userId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory LaporanUser.fromJson(String source) =>
      LaporanUser.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LaporanUser(id: $id, jumlah: $jumlah, tanggal: $tanggal, kategori: $kategori, nama: $nama, userId: $userId)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LaporanUser &&
        other.id == id &&
        other.jumlah == jumlah &&
        other.tanggal == tanggal &&
        other.kategori == kategori &&
        other.nama == nama &&
        other.userId == userId;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        jumlah.hashCode ^
        tanggal.hashCode ^
        kategori.hashCode ^
        nama.hashCode ^
        userId.hashCode;
  }
}
