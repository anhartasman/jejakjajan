import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/ResponGlobal.dart';

import 'package:jejakjajan/domain/entities/ViewUserSetting.dart';

import 'package:jejakjajan/domain/repositories/UserRepository.dart';

import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';

/// Retrieves the current authentication status of the [User]
class InsertLaporanUserUseCase
    extends CompletableUseCase<InsertLaporanUserUseCaseParams> {
  LaporanUserRepository _LaporanUserRepository;

  UserRepository _UserRepository;
  InsertLaporanUserUseCase(
    this._LaporanUserRepository,
    this._UserRepository,
  );

  @override
  Future<Stream<Respon>> buildUseCaseStream(
      InsertLaporanUserUseCaseParams? params) async {
    final StreamController<Respon> controller = StreamController();
    try {
      var respon_current_user =
          await _UserRepository.getUserAccount(new ViewUserSetting());
      if (!respon_current_user.success) {
        throw (respon_current_user.error_msg);
      }
      var the_current_user = respon_current_user.the_respon;
      _LaporanUserRepository.insertData(the_current_user!, params!.isiForm)
          .then((theRespon) {
        controller.add(theRespon);

        logger.finest('InsertLaporanUserUseCase successful.');
        controller.close();
      });
    } catch (e) {
      print(e);
      logger.severe('InsertLaporanUserUseCase unsuccessful.');
      controller.addError(e);
    }
    return controller.stream;
  }
}

class InsertLaporanUserUseCaseParams {
  IsiFormLaporan isiForm;
  InsertLaporanUserUseCaseParams(this.isiForm);
}
