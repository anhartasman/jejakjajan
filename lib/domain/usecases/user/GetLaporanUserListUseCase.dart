import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/entities/ResponGlobal.dart';

import 'package:jejakjajan/domain/entities/ViewUserSetting.dart';

import 'package:jejakjajan/domain/repositories/UserRepository.dart';

import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';

/// Retrieves the current authentication status of the [User]
class GetLaporanUserListUseCase
    extends CompletableUseCase<GetLaporanUserListUseCaseParams> {
  LaporanUserRepository _LaporanUserRepository;

  UserRepository _UserRepository;
  GetLaporanUserListUseCase(
    this._LaporanUserRepository,
    this._UserRepository,
  );

  @override
  Future<Stream<LaporanUser>> buildUseCaseStream(
      GetLaporanUserListUseCaseParams? params) async {
    final StreamController<LaporanUser> controller = StreamController();
    try {
      var respon_current_user =
          await _UserRepository.getUserAccount(new ViewUserSetting());
      if (!respon_current_user.success) {
        throw (respon_current_user.error_msg);
      }
      var the_current_user = respon_current_user.the_respon;

      _LaporanUserRepository.getLaporanUserList(
              the_current_user!, params!.theFilter)
          .then((rumah_list) {
        print("get " + rumah_list.length.toString() + " LaporanUser");

        rumah_list.forEach((theRumah) {
          controller.add(theRumah);
        });
        logger.finest('GetLaporanUserListUseCase successful.');
        controller.close();
      });
    } catch (e) {
      print(e);
      logger.severe('GetLaporanUserListUseCase unsuccessful.');
      controller.addError(e);
    }
    return controller.stream;
  }
}

class GetLaporanUserListUseCaseParams {
  FilterLaporan theFilter;
  GetLaporanUserListUseCaseParams(this.theFilter);
}
