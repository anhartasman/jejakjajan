import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/ResponGlobal.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';

abstract class LaporanUserRepository {
  Future<List<LaporanUser>> getLaporanUserList(
      UserAplikasi the_user, FilterLaporan theFilter);
  Future<Respon> insertData(UserAplikasi the_user, IsiFormLaporan isiForm);
}
