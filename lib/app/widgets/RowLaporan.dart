import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jejakjajan/app/display/InfoLaporanDisplay.dart';
import 'package:jejakjajan/app/utils/Warna.dart';
import 'package:intl/intl.dart';

class RowLaporan extends StatelessWidget {
  final InfoLaporanDisplay theLaporanDisplay;
  const RowLaporan({
    Key? key,
    required this.theLaporanDisplay,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 4.5,
              spreadRadius: 2.0,
              offset: Offset(
                0,
                3,
              ),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 15.0,
            vertical: 15,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Image(
                  color: Color(
                      int.parse(theLaporanDisplay.theKategori.icon_hex_color)),
                  image: AssetImage(theLaporanDisplay.theKategori.icon_path),
                ),
              ),
              Expanded(
                child: Text(
                  "${theLaporanDisplay.theLaporanUser.nama}",
                  style: TextStyle(
                    fontSize: 14.0,
                    fontFamily: "Popins",
                    color: Colors.black,
                  ),
                ),
              ),
              Text(
                "${NumberFormat.currency(locale: 'id', name: "Rp ", decimalDigits: 0).format(theLaporanDisplay.theLaporanUser.jumlah)}",
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: "Popins",
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
