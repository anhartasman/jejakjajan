import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:flutter/foundation.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';
import 'package:jejakjajan/domain/repositories/UserRepository.dart';

import 'package:jejakjajan/domain/usecases/user/GetLaporanUserListUseCase.dart';

class list_absensi_presenter extends Presenter {
  LaporanUserRepository _LaporanUserRepository;
  UserRepository _UserRepository;
  //auto_darttecture_class_var_declaration
//startPresenterUseCaseVarDeclarationForGetLaporanUserListUseCase
  late Function ListenUseCaseGetLaporanUserListUseCaseOnError;
  late Function ListenUseCaseGetLaporanUserListUseCaseOnNext;
  late Function ListenUseCaseGetLaporanUserListUseCaseOnComplete;
  GetLaporanUserListUseCase? _GetLaporanUserListUseCase;
//endPresenterUseCaseVarDeclarationForGetLaporanUserListUseCase
  list_absensi_presenter(
    this._LaporanUserRepository,
    this._UserRepository,
  ) {
    //auto_darttecture_usecase_initialize_repo
//start_usecase_initialize_repo_forGetLaporanUserListUseCase
    _GetLaporanUserListUseCase =
        GetLaporanUserListUseCase(_LaporanUserRepository, _UserRepository);
//end_usecase_initialize_repo_forGetLaporanUserListUseCase
  }
  void dispose() {
    //auto_darttecture_usecase_dispose
    _GetLaporanUserListUseCase?.dispose();
  }

//auto_darttecture_class_body
//startFunctionCallGetLaporanUserListUseCase
  void callGetLaporanUserListUseCase(FilterLaporan theFilter) {
    print("eksekusi GetLaporanUserListUseCase");
    _GetLaporanUserListUseCase?.execute(
        _GetLaporanUserListUseCaseObserver(this),
        GetLaporanUserListUseCaseParams(theFilter));
  }
//stopFunctionCallGetLaporanUserListUseCase
}

//auto_darttecture_class_outside
//startPresenterObserverUseCaseGetLaporanUserListUseCase
class _GetLaporanUserListUseCaseObserver implements Observer<LaporanUser> {
  // The above presenter
  list_absensi_presenter _list_absensi_presenter;
  _GetLaporanUserListUseCaseObserver(this._list_absensi_presenter);

  /// implement if the `Stream` emits a value
  void onNext(the_value) {
    if (_list_absensi_presenter.ListenUseCaseGetLaporanUserListUseCaseOnNext !=
        null) {
      _list_absensi_presenter.ListenUseCaseGetLaporanUserListUseCaseOnNext(
          the_value);
    }
  }

  /// Login is successfull, trigger event in [LoginController]
  void onComplete() {
    // any cleaning or preparation goes here
    if (_list_absensi_presenter
            .ListenUseCaseGetLaporanUserListUseCaseOnComplete !=
        null) {
      _list_absensi_presenter
          .ListenUseCaseGetLaporanUserListUseCaseOnComplete();
    }
  }

  /// Login was unsuccessful, trigger event in [LoginController]
  void onError(e) {
    // any cleaning or preparation goes here
    if (_list_absensi_presenter.ListenUseCaseGetLaporanUserListUseCaseOnError !=
        null) {
      _list_absensi_presenter.ListenUseCaseGetLaporanUserListUseCaseOnError(e);
    }
  }
}
//endPresenterObserverUseCaseGetLaporanUserListUseCase
