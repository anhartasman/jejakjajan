import 'package:flutter/animation.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';

import 'list_absensi_presenter.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//auto_darttecture_import_usecase_GetLaporanUserListUseCase
import 'package:jejakjajan/domain/repositories/UserRepository.dart';

class list_absensi_controller extends Controller {
  list_absensi_presenter _list_absensi_presenter;
  FilterLaporan theFilter;
  //auto_darttecture_class_var_declaration
//startControllerUseCaseVarDeclarationForGetLaporanUserListUseCase
  late Function(String errorMessageByUseCaseGetLaporanUserListUseCase)
      tambahan_callGetLaporanUserListUseCaseOnError;
  late Function(LaporanUser) tambahan_callGetLaporanUserListUseCaseOnNext;
  late Function tambahan_callGetLaporanUserListUseCaseOnComplete;
  var StartedUseCaseGetLaporanUserListUseCase = false;
  var StatusByUseCaseGetLaporanUserListUseCase = "idle";
  var MessageByUseCaseGetLaporanUserListUseCase = "";
//endControllerUseCaseVarDeclarationForGetLaporanUserListUseCase
  list_absensi_controller(
    this.theFilter,
    LaporanUserRepository _LaporanUserRepository,
    UserRepository _UserRepository,
  ) : _list_absensi_presenter = list_absensi_presenter(
          _LaporanUserRepository,
          _UserRepository,
        ) {
    //getAuthStatus();
    //countProductInCart();
    initListeners();
  }
  List<LaporanUser> absensi_list = [];
  @override
  void onInitState() {
    callGetLaporanUserListUseCase(
        theFilter: theFilter,
        onNext: (theAbsensi) {
          absensi_list.add(theAbsensi);
        },
        onComplete: () {
          stopLoadingInfoKlien();
        });
    Future.delayed(Duration(seconds: 1), () {
      stopLoading();
    });
  }

  bool onLoadingInfoKlien = true;
  void startLoadingInfoKlien() {
    onLoadingInfoKlien = true;
    notifyListeners();
  }

  void stopLoadingInfoKlien() {
    onLoadingInfoKlien = false;
    notifyListeners();
  }

  bool onLoading = true;
  void startLoading() {
    onLoading = true;
    notifyListeners();
  }

  void stopLoading() {
    onLoading = false;
    notifyListeners();
  }

  @override
  void initListeners() {
    //use_case_initListener
//startPresenterListenerOnUseCaseGetLaporanUserListUseCase
    _list_absensi_presenter.ListenUseCaseGetLaporanUserListUseCaseOnNext =
        (LaporanUser the_value) {
      this.ListenUseCaseGetLaporanUserListUseCaseOnNext(the_value);
    };
    _list_absensi_presenter.ListenUseCaseGetLaporanUserListUseCaseOnError =
        this.ListenUseCaseGetLaporanUserListUseCaseOnError;
    _list_absensi_presenter.ListenUseCaseGetLaporanUserListUseCaseOnComplete =
        this.ListenUseCaseGetLaporanUserListUseCaseOnComplete;
//stopPresenterListenerOnUseCaseGetLaporanUserListUseCase
  }

  void handlePermissions() {}
  void dispose() => _list_absensi_presenter.dispose();
//auto_darttecture_class_body
//startControllerCallUseCaseGetLaporanUserListUseCase
  static defaultFuncONNextGetLaporanUserListUseCase(LaporanUser theValue) {}
  static defaultFuncONErrorGetLaporanUserListUseCase(String errorMessage) {}
  static defaultFuncONCompleteGetLaporanUserListUseCase() {}
  void callGetLaporanUserListUseCase(
      {required FilterLaporan theFilter,
      Function(LaporanUser) onNext = defaultFuncONNextGetLaporanUserListUseCase,
      Function(String errorMessageByUseCaseGetLaporanUserListUseCase) onError =
          defaultFuncONErrorGetLaporanUserListUseCase,
      Function onComplete =
          defaultFuncONCompleteGetLaporanUserListUseCase}) async {
    tambahan_callGetLaporanUserListUseCaseOnNext = onNext;
    tambahan_callGetLaporanUserListUseCaseOnError = onError;
    tambahan_callGetLaporanUserListUseCaseOnComplete = onComplete;
    StatusByUseCaseGetLaporanUserListUseCase = "ongoing";
    StartedUseCaseGetLaporanUserListUseCase = true;
    //showLoading();
    // so the animation can be seen
    print("controller try callGetLaporanUserListUseCase");
    Future.delayed(Duration(seconds: 0),
        () => _list_absensi_presenter.callGetLaporanUserListUseCase(theFilter));
  }

//endControllerCallUseCaseGetLaporanUserListUseCase
//startListenerOnUseCaseGetLaporanUserListUseCase
  void ListenUseCaseGetLaporanUserListUseCaseOnNext(LaporanUser the_value) {
    //get called when usecase GetLaporanUserListUseCase return value
    //dismissLoading();
    StatusByUseCaseGetLaporanUserListUseCase = "onnext";
//startDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnNext
//endDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnNext
    if (tambahan_callGetLaporanUserListUseCaseOnNext != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetLaporanUserListUseCaseOnNext());
      tambahan_callGetLaporanUserListUseCaseOnNext(the_value);
    }
    //
    //print("dapat layanan : " + the_categories.length.toString());
    //refreshUI();
  }

  void ListenUseCaseGetLaporanUserListUseCaseOnError(e) {
    StatusByUseCaseGetLaporanUserListUseCase = "onerror";
    MessageByUseCaseGetLaporanUserListUseCase = e.toString();
    //get called when usecase GetLaporanUserListUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnError
//endDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnError
    if (tambahan_callGetLaporanUserListUseCaseOnError != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetLaporanUserListUseCaseonError());
      tambahan_callGetLaporanUserListUseCaseOnError(e);
    }
    //layanan_list = null;
    print("dapat error dari GetLaporanUserListUseCase");
    //refreshUI();
  }

  void ListenUseCaseGetLaporanUserListUseCaseOnComplete() {
    StatusByUseCaseGetLaporanUserListUseCase = "oncomplete";
    //get called when usecase GetLaporanUserListUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnComplete
//endDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnComplete
    if (tambahan_callGetLaporanUserListUseCaseOnComplete != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetLaporanUserListUseCaseonComplete());
      tambahan_callGetLaporanUserListUseCaseOnComplete();
    }
    //layanan_list = null;
    print("dapat oncomplete dari GetLaporanUserListUseCase");
    //refreshUI();
  }
//stopListenerOnUseCaseGetLaporanUserListUseCase
}
//auto_darttecture_class_outside
