import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:flutter/foundation.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';

import 'package:jejakjajan/domain/repositories/UserRepository.dart';
import 'package:jejakjajan/domain/usecases/user/GetLaporanUserListUseCase.dart';
import 'package:jejakjajan/domain/usecases/user/GetCurrentUserUseCase.dart';
import 'package:jejakjajan/domain/usecases/user/UserLogoutUseCase.dart';

import 'package:jejakjajan/domain/usecases/user/UserLogoutUseCase.dart';

class home_user_presenter extends Presenter {
  LaporanUserRepository _LaporanUserRepository;
  UserRepository _UserRepository;
  //auto_darttecture_class_var_declaration
//startPresenterUseCaseVarDeclarationForUserLogoutUseCase
  late Function ListenUseCaseUserLogoutUseCaseOnError;
  late Function ListenUseCaseUserLogoutUseCaseOnNext;
  late Function ListenUseCaseUserLogoutUseCaseOnComplete;
  UserLogoutUseCase? _UserLogoutUseCase;
//endPresenterUseCaseVarDeclarationForUserLogoutUseCase
  //startPresenterUseCaseVarDeclarationForGetCurrentUserUseCase
  late Function ListenUseCaseGetCurrentUserUseCaseOnError;
  late Function ListenUseCaseGetCurrentUserUseCaseOnNext;
  late Function ListenUseCaseGetCurrentUserUseCaseOnComplete;
  GetCurrentUserUseCase? _GetCurrentUserUseCase;
//endPresenterUseCaseVarDeclarationForGetCurrentUserUseCase
  //startPresenterUseCaseVarDeclarationForGetLaporanUserListUseCase
  late Function ListenUseCaseGetLaporanUserListUseCaseOnError;
  late Function ListenUseCaseGetLaporanUserListUseCaseOnNext;
  late Function ListenUseCaseGetLaporanUserListUseCaseOnComplete;
  GetLaporanUserListUseCase? _GetLaporanUserListUseCase;
//endPresenterUseCaseVarDeclarationForGetLaporanUserListUseCase
  home_user_presenter(
    this._LaporanUserRepository,
    this._UserRepository,
  ) {
    //auto_darttecture_usecase_initialize_repo
//start_usecase_initialize_repo_forUserLogoutUseCase
    _UserLogoutUseCase = UserLogoutUseCase(_UserRepository);
//end_usecase_initialize_repo_forUserLogoutUseCase
    //start_usecase_initialize_repo_forGetCurrentUserUseCase
    _GetCurrentUserUseCase = GetCurrentUserUseCase(_UserRepository);
//end_usecase_initialize_repo_forGetCurrentUserUseCase
    //start_usecase_initialize_repo_forGetLaporanUserListUseCase
    _GetLaporanUserListUseCase =
        GetLaporanUserListUseCase(_LaporanUserRepository, _UserRepository);
//end_usecase_initialize_repo_forGetLaporanUserListUseCase
  }
  void dispose() {
    //auto_darttecture_usecase_dispose
    _UserLogoutUseCase?.dispose();
    _GetCurrentUserUseCase?.dispose();
    _GetLaporanUserListUseCase?.dispose();
  }

//auto_darttecture_class_body
//startFunctionCallUserLogoutUseCase
  void callUserLogoutUseCase() {
    print("eksekusi UserLogoutUseCase");
    _UserLogoutUseCase?.execute(
        _UserLogoutUseCaseObserver(this), UserLogoutUseCaseParams());
  }

//stopFunctionCallUserLogoutUseCase
//startFunctionCallGetCurrentUserUseCase
  void callGetCurrentUserUseCase() {
    print("eksekusi GetCurrentUserUseCase");
    _GetCurrentUserUseCase?.execute(
        _GetCurrentUserUseCaseObserver(this), GetCurrentUserUseCaseParams());
  }

//stopFunctionCallGetCurrentUserUseCase
//startFunctionCallGetLaporanUserListUseCase
  void callGetLaporanUserListUseCase(FilterLaporan theFilter) {
    print("eksekusi GetLaporanUserListUseCase");
    _GetLaporanUserListUseCase?.execute(
        _GetLaporanUserListUseCaseObserver(this),
        GetLaporanUserListUseCaseParams(theFilter));
  }
//stopFunctionCallGetLaporanUserListUseCase
}

//auto_darttecture_class_outside
//startPresenterObserverUseCaseUserLogoutUseCase
class _UserLogoutUseCaseObserver implements Observer<Respon> {
  // The above presenter
  home_user_presenter _home_user_presenter;
  _UserLogoutUseCaseObserver(this._home_user_presenter);

  /// implement if the `Stream` emits a value
  void onNext(the_value) {
    if (_home_user_presenter.ListenUseCaseUserLogoutUseCaseOnNext != null) {
      _home_user_presenter.ListenUseCaseUserLogoutUseCaseOnNext(the_value);
    }
  }

  /// Login is successfull, trigger event in [LoginController]
  void onComplete() {
    // any cleaning or preparation goes here
    if (_home_user_presenter.ListenUseCaseUserLogoutUseCaseOnComplete != null) {
      _home_user_presenter.ListenUseCaseUserLogoutUseCaseOnComplete();
    }
  }

  /// Login was unsuccessful, trigger event in [LoginController]
  void onError(e) {
    // any cleaning or preparation goes here
    if (_home_user_presenter.ListenUseCaseUserLogoutUseCaseOnError != null) {
      _home_user_presenter.ListenUseCaseUserLogoutUseCaseOnError(e);
    }
  }
}

//endPresenterObserverUseCaseUserLogoutUseCase
//startPresenterObserverUseCaseGetCurrentUserUseCase
class _GetCurrentUserUseCaseObserver implements Observer<UserAplikasi> {
  // The above presenter
  home_user_presenter _home_user_presenter;
  _GetCurrentUserUseCaseObserver(this._home_user_presenter);

  /// implement if the `Stream` emits a value
  void onNext(the_value) {
    if (_home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnNext != null) {
      _home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnNext(the_value);
    }
  }

  /// Login is successfull, trigger event in [LoginController]
  void onComplete() {
    // any cleaning or preparation goes here
    if (_home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnComplete !=
        null) {
      _home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnComplete();
    }
  }

  /// Login was unsuccessful, trigger event in [LoginController]
  void onError(e) {
    // any cleaning or preparation goes here
    if (_home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnError !=
        null) {
      _home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnError(e);
    }
  }
}

//endPresenterObserverUseCaseGetCurrentUserUseCase
//startPresenterObserverUseCaseGetLaporanUserListUseCase
class _GetLaporanUserListUseCaseObserver implements Observer<LaporanUser> {
  // The above presenter
  home_user_presenter _home_user_presenter;
  _GetLaporanUserListUseCaseObserver(this._home_user_presenter);

  /// implement if the `Stream` emits a value
  void onNext(the_value) {
    if (_home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnNext !=
        null) {
      _home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnNext(
          the_value);
    }
  }

  /// Login is successfull, trigger event in [LoginController]
  void onComplete() {
    // any cleaning or preparation goes here
    if (_home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnComplete !=
        null) {
      _home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnComplete();
    }
  }

  /// Login was unsuccessful, trigger event in [LoginController]
  void onError(e) {
    // any cleaning or preparation goes here
    if (_home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnError !=
        null) {
      _home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnError(e);
    }
  }
}
//endPresenterObserverUseCaseGetLaporanUserListUseCase
