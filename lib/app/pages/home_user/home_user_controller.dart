import 'package:flutter/animation.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:jejakjajan/app/pages/akun_login/akun_login_view.dart';
import 'package:jejakjajan/app/pages/form_laporan/form_laporan_view.dart';
import 'package:jejakjajan/app/pages/home_user/home_user_view.dart';
import 'package:jejakjajan/app/pages/utils/TampilanDialog.dart';
import 'package:jejakjajan/app/utils/constants.dart';
import 'package:jejakjajan/domain/entities/KategoriLaporan.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';

import 'package:place_picker/place_picker.dart';
import 'home_user_presenter.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//auto_darttecture_import_usecase_UserLogoutUseCase
//auto_darttecture_import_usecase_GetCurrentUserUseCase
//auto_darttecture_import_usecase_GetLaporanUserListUseCase
import 'package:jejakjajan/domain/repositories/UserRepository.dart';

class home_user_controller extends Controller {
  home_user_presenter _home_user_presenter;
  //auto_darttecture_class_var_declaration
//startControllerUseCaseVarDeclarationForUserLogoutUseCase
  late Function(String errorMessageByUseCaseUserLogoutUseCase)
      tambahan_callUserLogoutUseCaseOnError;
  late Function(Respon) tambahan_callUserLogoutUseCaseOnNext;
  late Function tambahan_callUserLogoutUseCaseOnComplete;
  var StartedUseCaseUserLogoutUseCase = false;
  var StatusByUseCaseUserLogoutUseCase = "idle";
  var MessageByUseCaseUserLogoutUseCase = "";
//endControllerUseCaseVarDeclarationForUserLogoutUseCase
  //startControllerUseCaseVarDeclarationForGetCurrentUserUseCase
  late Function(String errorMessageByUseCaseGetCurrentUserUseCase)
      tambahan_callGetCurrentUserUseCaseOnError;
  late Function(UserAplikasi) tambahan_callGetCurrentUserUseCaseOnNext;
  late Function tambahan_callGetCurrentUserUseCaseOnComplete;
  var StartedUseCaseGetCurrentUserUseCase = false;
  var StatusByUseCaseGetCurrentUserUseCase = "idle";
  var MessageByUseCaseGetCurrentUserUseCase = "";
//endControllerUseCaseVarDeclarationForGetCurrentUserUseCase
  //startControllerUseCaseVarDeclarationForGetLaporanUserListUseCase
  late Function(String errorMessageByUseCaseGetLaporanUserListUseCase)
      tambahan_callGetLaporanUserListUseCaseOnError;
  late Function(LaporanUser) tambahan_callGetLaporanUserListUseCaseOnNext;
  late Function tambahan_callGetLaporanUserListUseCaseOnComplete;
  var StartedUseCaseGetLaporanUserListUseCase = false;
  var StatusByUseCaseGetLaporanUserListUseCase = "idle";
  var MessageByUseCaseGetLaporanUserListUseCase = "";
//endControllerUseCaseVarDeclarationForGetLaporanUserListUseCase
  home_user_controller(
    LaporanUserRepository _LaporanUserRepository,
    UserRepository _UserRepository,
  ) : _home_user_presenter = home_user_presenter(
          _LaporanUserRepository,
          _UserRepository,
        ) {
    //getAuthStatus();
    //countProductInCart();
    initListeners();
  }

  late UserAplikasi theUser;
  List<LaporanUser> absen_all_list = [];
  List<LaporanUser> absen_day_list = [];
  List<LaporanUser> absen_month_list = [];
  int totalPengeluaranBulanan = 0;
  int totalPengeluaranHariIni = 0;

  @override
  void onInitState() {
    resetGroupList();
    callGetCurrentUserUseCase(
      onNext: (user) {
        theUser = user;
        ambilData();
      },
    );
  }

  Map<KategoriLaporan, List<LaporanUser>> group_list = {};

  void resetGroupList() {
    group_list.clear();
    ConstKategori.All.forEach((theKategori) {
      group_list[theKategori] = [];
    });
  }

  void bukaForm() {
    Get.to(form_laporan_view())?.then((value) => ambilData());
  }

  void ambilData() {
    absen_all_list.clear();
    absen_day_list.clear();
    absen_month_list.clear();
    var dateAllFrom = DateTime.now();
    var dateAllTo = DateTime.now();
    var dateMonthFrom = DateTime.now().subtract(Duration(days: 30));
    var dateMonthTo = DateTime.now();
    var dateDayFrom = DateTime.now().subtract(Duration(days: 1));
    var dateDayTo = DateTime.now();

    //mengambil laporan seluruhnya
    callGetLaporanUserListUseCase(
        theFilter: FilterLaporan(
            all: true,
            dateFrom: dateAllFrom.millisecondsSinceEpoch,
            dateTo: dateAllTo.millisecondsSinceEpoch),
        onNext: (theAbsensiAll) {
          absen_all_list.add(theAbsensiAll);
          group_list.keys.forEach((groupKey) {
            if (groupKey.id == theAbsensiAll.kategori) {
              final cariKategori =
                  ConstKategori.cariById(theAbsensiAll.kategori);
              if (cariKategori.success) {
                final hasilCariKategori = cariKategori.the_respon!;
                group_list[hasilCariKategori]!.add(theAbsensiAll);
              }
            }
          });
        },
        onComplete: () {
          //mengambil laporan bulan ini
          totalPengeluaranBulanan = 0;
          callGetLaporanUserListUseCase(
            theFilter: FilterLaporan(
                dateFrom: dateMonthFrom.millisecondsSinceEpoch,
                dateTo: dateMonthTo.millisecondsSinceEpoch),
            onNext: (theAbsensiMonth) {
              totalPengeluaranBulanan += theAbsensiMonth.jumlah;
              absen_month_list.add(theAbsensiMonth);
            },
            onComplete: () {
              //mengambil laporan hari ini
              totalPengeluaranHariIni = 0;
              callGetLaporanUserListUseCase(
                theFilter: FilterLaporan(
                    dateFrom: dateDayFrom.millisecondsSinceEpoch,
                    dateTo: dateDayTo.millisecondsSinceEpoch),
                onNext: (theAbsensiDay) {
                  totalPengeluaranHariIni += theAbsensiDay.jumlah;
                  absen_day_list.add(theAbsensiDay);
                },
                onComplete: stopLoading,
              );
            },
          );
        });
  }

  bool onLoading = true;
  void startLoading() {
    onLoading = true;
    notifyListeners();
  }

  void stopLoading() {
    onLoading = false;
    notifyListeners();
  }

  final formKey = GlobalKey<FormBuilderState>();

  @override
  void initListeners() {
    //use_case_initListener
//startPresenterListenerOnUseCaseUserLogoutUseCase
    _home_user_presenter.ListenUseCaseUserLogoutUseCaseOnNext =
        (Respon the_value) {
      this.ListenUseCaseUserLogoutUseCaseOnNext(the_value);
    };
    _home_user_presenter.ListenUseCaseUserLogoutUseCaseOnError =
        this.ListenUseCaseUserLogoutUseCaseOnError;
    _home_user_presenter.ListenUseCaseUserLogoutUseCaseOnComplete =
        this.ListenUseCaseUserLogoutUseCaseOnComplete;
//stopPresenterListenerOnUseCaseUserLogoutUseCase
//startPresenterListenerOnUseCaseGetCurrentUserUseCase
    _home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnNext =
        (UserAplikasi the_value) {
      this.ListenUseCaseGetCurrentUserUseCaseOnNext(the_value);
    };
    _home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnError =
        this.ListenUseCaseGetCurrentUserUseCaseOnError;
    _home_user_presenter.ListenUseCaseGetCurrentUserUseCaseOnComplete =
        this.ListenUseCaseGetCurrentUserUseCaseOnComplete;
//stopPresenterListenerOnUseCaseGetCurrentUserUseCase
    //startPresenterListenerOnUseCaseGetLaporanUserListUseCase
    _home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnNext =
        (LaporanUser the_value) {
      this.ListenUseCaseGetLaporanUserListUseCaseOnNext(the_value);
    };
    _home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnError =
        this.ListenUseCaseGetLaporanUserListUseCaseOnError;
    _home_user_presenter.ListenUseCaseGetLaporanUserListUseCaseOnComplete =
        this.ListenUseCaseGetLaporanUserListUseCaseOnComplete;
//stopPresenterListenerOnUseCaseGetLaporanUserListUseCase
  }

  BuildContext? dialogContext;
  void handlePermissions() {}
  void dispose() => _home_user_presenter.dispose();
  void prosesLogout() {
    callUserLogoutUseCase(onComplete: () {
      Get.offAll(akun_login_view());
    });
  }

//auto_darttecture_class_body
//startControllerCallUseCaseUserLogoutUseCase
  static defaultFuncONNextUserLogoutUseCase(Respon theValue) {}
  static defaultFuncONErrorUserLogoutUseCase(String errorMessage) {}
  static defaultFuncONCompleteUserLogoutUseCase() {}
  void callUserLogoutUseCase(
      {Function(Respon) onNext = defaultFuncONNextUserLogoutUseCase,
      Function(String errorMessageByUseCaseUserLogoutUseCase) onError =
          defaultFuncONErrorUserLogoutUseCase,
      Function onComplete = defaultFuncONCompleteUserLogoutUseCase}) async {
    tambahan_callUserLogoutUseCaseOnNext = onNext;
    tambahan_callUserLogoutUseCaseOnError = onError;
    tambahan_callUserLogoutUseCaseOnComplete = onComplete;
    StatusByUseCaseUserLogoutUseCase = "ongoing";
    StartedUseCaseUserLogoutUseCase = true;
    //showLoading();
    // so the animation can be seen
    print("controller try callUserLogoutUseCase");
    Future.delayed(Duration(seconds: 0),
        () => _home_user_presenter.callUserLogoutUseCase());
  }

//endControllerCallUseCaseUserLogoutUseCase
//startListenerOnUseCaseUserLogoutUseCase
  void ListenUseCaseUserLogoutUseCaseOnNext(Respon the_value) {
    //get called when usecase UserLogoutUseCase return value
    //dismissLoading();
    StatusByUseCaseUserLogoutUseCase = "onnext";
//startDefaultFunctionOnListenUseCaseUserLogoutUseCaseOnNext
//endDefaultFunctionOnListenUseCaseUserLogoutUseCaseOnNext
    if (tambahan_callUserLogoutUseCaseOnNext != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callUserLogoutUseCaseOnNext());
      tambahan_callUserLogoutUseCaseOnNext(the_value);
    }
    //
    //print("dapat layanan : " + the_categories.length.toString());
    //refreshUI();
  }

  void ListenUseCaseUserLogoutUseCaseOnError(e) {
    StatusByUseCaseUserLogoutUseCase = "onerror";
    MessageByUseCaseUserLogoutUseCase = e.toString();
    //get called when usecase UserLogoutUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseUserLogoutUseCaseOnError
//endDefaultFunctionOnListenUseCaseUserLogoutUseCaseOnError
    if (tambahan_callUserLogoutUseCaseOnError != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callUserLogoutUseCaseonError());
      tambahan_callUserLogoutUseCaseOnError(e);
    }
    //layanan_list = null;
    print("dapat error dari UserLogoutUseCase");
    //refreshUI();
  }

  void ListenUseCaseUserLogoutUseCaseOnComplete() {
    StatusByUseCaseUserLogoutUseCase = "oncomplete";
    //get called when usecase UserLogoutUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseUserLogoutUseCaseOnComplete
//endDefaultFunctionOnListenUseCaseUserLogoutUseCaseOnComplete
    if (tambahan_callUserLogoutUseCaseOnComplete != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callUserLogoutUseCaseonComplete());
      tambahan_callUserLogoutUseCaseOnComplete();
    }
    //layanan_list = null;
    print("dapat oncomplete dari UserLogoutUseCase");
    //refreshUI();
  }

//stopListenerOnUseCaseUserLogoutUseCase
//startControllerCallUseCaseGetCurrentUserUseCase
  static defaultFuncONNextGetCurrentUserUseCase(UserAplikasi theValue) {}
  static defaultFuncONErrorGetCurrentUserUseCase(String errorMessage) {}
  static defaultFuncONCompleteGetCurrentUserUseCase() {}
  void callGetCurrentUserUseCase(
      {Function(UserAplikasi) onNext = defaultFuncONNextGetCurrentUserUseCase,
      Function(String errorMessageByUseCaseGetCurrentUserUseCase) onError =
          defaultFuncONErrorGetCurrentUserUseCase,
      Function onComplete = defaultFuncONCompleteGetCurrentUserUseCase}) async {
    tambahan_callGetCurrentUserUseCaseOnNext = onNext;
    tambahan_callGetCurrentUserUseCaseOnError = onError;
    tambahan_callGetCurrentUserUseCaseOnComplete = onComplete;
    StatusByUseCaseGetCurrentUserUseCase = "ongoing";
    StartedUseCaseGetCurrentUserUseCase = true;
    //showLoading();
    // so the animation can be seen
    print("controller try callGetCurrentUserUseCase");
    Future.delayed(Duration(seconds: 0),
        () => _home_user_presenter.callGetCurrentUserUseCase());
  }

//endControllerCallUseCaseGetCurrentUserUseCase
//startListenerOnUseCaseGetCurrentUserUseCase
  void ListenUseCaseGetCurrentUserUseCaseOnNext(UserAplikasi the_value) {
    //get called when usecase GetCurrentUserUseCase return value
    //dismissLoading();
    StatusByUseCaseGetCurrentUserUseCase = "onnext";
//startDefaultFunctionOnListenUseCaseGetCurrentUserUseCaseOnNext
//endDefaultFunctionOnListenUseCaseGetCurrentUserUseCaseOnNext
    if (tambahan_callGetCurrentUserUseCaseOnNext != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetCurrentUserUseCaseOnNext());
      tambahan_callGetCurrentUserUseCaseOnNext(the_value);
    }
    //
    //print("dapat layanan : " + the_categories.length.toString());
    //refreshUI();
  }

  void ListenUseCaseGetCurrentUserUseCaseOnError(e) {
    StatusByUseCaseGetCurrentUserUseCase = "onerror";
    MessageByUseCaseGetCurrentUserUseCase = e.toString();
    //get called when usecase GetCurrentUserUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseGetCurrentUserUseCaseOnError
//endDefaultFunctionOnListenUseCaseGetCurrentUserUseCaseOnError
    if (tambahan_callGetCurrentUserUseCaseOnError != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetCurrentUserUseCaseonError());
      tambahan_callGetCurrentUserUseCaseOnError(e);
    }
    //layanan_list = null;
    print("dapat error dari GetCurrentUserUseCase");
    //refreshUI();
  }

  void ListenUseCaseGetCurrentUserUseCaseOnComplete() {
    StatusByUseCaseGetCurrentUserUseCase = "oncomplete";
    //get called when usecase GetCurrentUserUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseGetCurrentUserUseCaseOnComplete
//endDefaultFunctionOnListenUseCaseGetCurrentUserUseCaseOnComplete
    if (tambahan_callGetCurrentUserUseCaseOnComplete != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetCurrentUserUseCaseonComplete());
      tambahan_callGetCurrentUserUseCaseOnComplete();
    }
    //layanan_list = null;
    print("dapat oncomplete dari GetCurrentUserUseCase");
    //refreshUI();
  }
//stopListenerOnUseCaseGetCurrentUserUseCase

//startControllerCallUseCaseGetLaporanUserListUseCase
  static defaultFuncONNextGetLaporanUserListUseCase(LaporanUser theValue) {}
  static defaultFuncONErrorGetLaporanUserListUseCase(String errorMessage) {}
  static defaultFuncONCompleteGetLaporanUserListUseCase() {}
  void callGetLaporanUserListUseCase(
      {required FilterLaporan theFilter,
      Function(LaporanUser) onNext = defaultFuncONNextGetLaporanUserListUseCase,
      Function(String errorMessageByUseCaseGetLaporanUserListUseCase) onError =
          defaultFuncONErrorGetLaporanUserListUseCase,
      Function onComplete =
          defaultFuncONCompleteGetLaporanUserListUseCase}) async {
    tambahan_callGetLaporanUserListUseCaseOnNext = onNext;
    tambahan_callGetLaporanUserListUseCaseOnError = onError;
    tambahan_callGetLaporanUserListUseCaseOnComplete = onComplete;
    StatusByUseCaseGetLaporanUserListUseCase = "ongoing";
    StartedUseCaseGetLaporanUserListUseCase = true;
    //showLoading();
    // so the animation can be seen
    print("controller try callGetLaporanUserListUseCase");
    Future.delayed(Duration(seconds: 0),
        () => _home_user_presenter.callGetLaporanUserListUseCase(theFilter));
  }

//endControllerCallUseCaseGetLaporanUserListUseCase
//startListenerOnUseCaseGetLaporanUserListUseCase
  void ListenUseCaseGetLaporanUserListUseCaseOnNext(LaporanUser the_value) {
    //get called when usecase GetLaporanUserListUseCase return value
    //dismissLoading();
    StatusByUseCaseGetLaporanUserListUseCase = "onnext";
//startDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnNext
//endDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnNext
    if (tambahan_callGetLaporanUserListUseCaseOnNext != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetLaporanUserListUseCaseOnNext());
      tambahan_callGetLaporanUserListUseCaseOnNext(the_value);
    }
    //
    //print("dapat layanan : " + the_categories.length.toString());
    //refreshUI();
  }

  void ListenUseCaseGetLaporanUserListUseCaseOnError(e) {
    StatusByUseCaseGetLaporanUserListUseCase = "onerror";
    MessageByUseCaseGetLaporanUserListUseCase = e.toString();
    //get called when usecase GetLaporanUserListUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnError
//endDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnError
    if (tambahan_callGetLaporanUserListUseCaseOnError != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetLaporanUserListUseCaseonError());
      tambahan_callGetLaporanUserListUseCaseOnError(e);
    }
    //layanan_list = null;
    print("dapat error dari GetLaporanUserListUseCase");
    //refreshUI();
  }

  void ListenUseCaseGetLaporanUserListUseCaseOnComplete() {
    StatusByUseCaseGetLaporanUserListUseCase = "oncomplete";
    //get called when usecase GetLaporanUserListUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnComplete
//endDefaultFunctionOnListenUseCaseGetLaporanUserListUseCaseOnComplete
    if (tambahan_callGetLaporanUserListUseCaseOnComplete != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callGetLaporanUserListUseCaseonComplete());
      tambahan_callGetLaporanUserListUseCaseOnComplete();
    }
    //layanan_list = null;
    print("dapat oncomplete dari GetLaporanUserListUseCase");
    //refreshUI();
  }
//stopListenerOnUseCaseGetLaporanUserListUseCase
}
//auto_darttecture_class_outside
