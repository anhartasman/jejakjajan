import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jejakjajan/app/pages/utils/GayaField.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:get/get.dart';
import 'package:jejakjajan/app/utils/Warna.dart';
import 'package:jejakjajan/app/utils/constants.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'form_laporan_controller.dart';
import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:jejakjajan/data/repositories/DataLaporanUserRepository.dart';
import 'package:jejakjajan/data/repositories/DataUserRepository.dart';

class form_laporan_view extends View {
  form_laporan_view();
  @override
  form_laporan_viewView createState() =>
      form_laporan_viewView(form_laporan_controller(
        new DataLaporanUserRepository(),
        new DataUserRepository(),
      ));
}

class form_laporan_viewView
    extends ViewState<form_laporan_view, form_laporan_controller>
    with TickerProviderStateMixin, AfterLayoutMixin<form_laporan_view> {
  form_laporan_viewView(form_laporan_controller controller) : super(controller);
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget get view {
    return Scaffold(
        key: globalKey,
        appBar: AppBar(
          brightness: Brightness.dark,
          title: const Text('Tambah Pengeluaran Baru'),
          bottomOpacity: 0.0,
          elevation: 0.0,
          leading: IconButton(
            icon: const FaIcon(FontAwesomeIcons.chevronLeft),
            tooltip: 'Back',
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: ControlledWidgetBuilder<form_laporan_controller>(
            builder: (context, controller) {
          if (controller.onLoading) {
            return SpinKitWave(
              color: Colors.pink,
              size: 50.0,
              controller: AnimationController(
                  vsync: this, duration: const Duration(milliseconds: 1200)),
            );
          }
          return SlidingUpPanel(
            maxHeight: ScreenUtil().screenHeight / 2,
            minHeight: 0,
            controller: controller.pc,
            panel: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15.0,
                      vertical: 15,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Pilih Kategori",
                            style: TextStyle(
                              fontSize: 20.0,
                              fontFamily: "Popins",
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.pc.close();
                          },
                          child:
                              Icon(Icons.close, size: 25, color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  GridView.count(
                    physics: const NeverScrollableScrollPhysics(),
                    childAspectRatio: 1,
                    shrinkWrap: true,
                    crossAxisCount: 3,
                    padding: EdgeInsets.symmetric(vertical: 0),
                    children: ConstKategori.All.map((item) => InkWell(
                          onTap: () => controller.pilihKategori(item),
                          child: Container(
                              child: Column(
                            children: [
                              new Center(
                                child: new CircleAvatar(
                                  backgroundColor:
                                      Color(int.parse(item.icon_hex_color)),
                                  child: new Image(
                                      image: AssetImage(item.icon_path)),
                                ),
                              ),
                              Text(
                                "${item.nama}",
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black,
                                  fontFamily: "Poppins",
                                ),
                              ),
                            ],
                          )),
                        )).toList(),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 65.0),
                  ),
                ],
              ),
            ),
            body: SingleChildScrollView(
              child: FormBuilder(
                key: controller.formKey,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          child: new FormBuilderTextField(
                            name: "namaPengeluaran",
                            decoration: GayaField.putih(
                              "Nama Pengeluaran",
                            ),
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(context),
                            ]),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          child: new FormBuilderTextField(
                            name: "kategori",
                            readOnly: true,
                            onTap: controller.bukaPilihanKategori,
                            controller: controller.txtCtrlLabelKategori,
                            decoration: GayaField.putih(
                              null,
                              prefix: Padding(
                                padding: const EdgeInsets.only(right: 5.0),
                                child: Image(
                                  color: Color(int.parse(controller
                                      .selectedKategori.icon_hex_color)),
                                  image: AssetImage(
                                      controller.selectedKategori.icon_path),
                                ),
                              ),
                              suffixIcon: Icon(Icons.chevron_right_outlined),
                            ),
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(context),
                            ]),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          child: new FormBuilderTextField(
                            name: "tanggalPengeluaran",
                            readOnly: true,
                            onTap: controller.cariDateFrom,
                            controller: controller.txtCtrlLabelTanggal,
                            decoration: GayaField.putih(
                              "Tanggal Pengeluaran",
                              suffixIcon: Icon(Icons.calendar_today),
                            ),
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(context),
                            ]),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          child: new FormBuilderTextField(
                            name: "jumlahPengeluaran",
                            decoration: GayaField.putih(
                              "Nominal",
                            ),
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(context),
                            ]),
                            keyboardType: TextInputType.number,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 50.0),
                          child: InkWell(
                            onTap: controller.simpanLaporan,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Warna.warnaUtama,
                                borderRadius: new BorderRadius.circular(8.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Center(
                                  child: Text(
                                    "Simpan",
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontFamily: "Poppins",
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }));
  }

  void onDonePress() {
    // Do what you want
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => HomeScreen()),
    // );
  }
  @override
  void afterFirstLayout(BuildContext context) async {}
}
