import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:jejakjajan/app/pages/home_user/home_user_view.dart';
import 'package:jejakjajan/app/pages/utils/TampilanDialog.dart';
import 'package:jejakjajan/app/utils/constants.dart';
import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/KategoriLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';

import 'package:place_picker/place_picker.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'form_laporan_presenter.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//auto_darttecture_import_usecase_InsertLaporanUserUseCase
import 'package:jejakjajan/domain/repositories/UserRepository.dart';

class form_laporan_controller extends Controller {
  form_laporan_presenter _form_laporan_presenter;
  //auto_darttecture_class_var_declaration
//startControllerUseCaseVarDeclarationForInsertLaporanUserUseCase
  late Function(String errorMessageByUseCaseInsertLaporanUserUseCase)
      tambahan_callInsertLaporanUserUseCaseOnError;
  late Function(Respon) tambahan_callInsertLaporanUserUseCaseOnNext;
  late Function tambahan_callInsertLaporanUserUseCaseOnComplete;
  var StartedUseCaseInsertLaporanUserUseCase = false;
  var StatusByUseCaseInsertLaporanUserUseCase = "idle";
  var MessageByUseCaseInsertLaporanUserUseCase = "";
//endControllerUseCaseVarDeclarationForInsertLaporanUserUseCase
  form_laporan_controller(
    LaporanUserRepository _LaporanUserRepository,
    UserRepository _UserRepository,
  ) : _form_laporan_presenter = form_laporan_presenter(
          _LaporanUserRepository,
          _UserRepository,
        ) {
    //getAuthStatus();
    //countProductInCart();
    initListeners();
  }
  var selectedKategori = ConstKategori.makanan;

  void bukaPilihanKategori() {
    pc.open();
  }

  void pilihKategori(KategoriLaporan newSelected) {
    isiForm.kategori = newSelected.id;
    txtCtrlLabelKategori.text = newSelected.nama;
    selectedKategori = newSelected;
    pc.close();
    refreshUI();
  }

  var isiForm = IsiFormLaporan(
    jumlah: 0,
    tanggal: DateTime.now().millisecondsSinceEpoch,
    kategori: ConstKategori.makanan.id,
    nama: "",
  );

  PanelController pc = new PanelController();
  var txtCtrlLabelKategori = new TextEditingController();
  var txtCtrlLabelTanggal = new TextEditingController();
  String labelTanggal = "";

  var dateReport = DateTime.now().subtract(Duration(days: 30));

  void cariDateFrom() {
    showRoundedDatePicker(
      context: getContext(),
      locale: Locale("id", "ID"),
      initialDate: dateReport,
      firstDate: DateTime(DateTime.now().year - 20),
      lastDate: DateTime.now(),
      borderRadius: 16,
    ).then((newDateTime) {
      if (newDateTime != null) {
        dateReport = newDateTime;
        final tanggal_pilihan =
            DateFormat(formatTanggal, "id_ID").format(newDateTime);
        txtCtrlLabelTanggal.text = tanggal_pilihan;
        isiForm.tanggal = dateReport.millisecondsSinceEpoch;
      }
    });
  }

  void simpanLaporan() {
    if (!formKey.currentState!.saveAndValidate()) {
      TampilanDialog.alertDialog("Maaf", "Lengkapi form terlebih dahulu");
    } else {
      isiForm.nama =
          formKey.currentState!.fields["namaPengeluaran"]!.value as String;
      isiForm.jumlah = int.parse(
          formKey.currentState!.fields["jumlahPengeluaran"]!.value as String);
      callInsertLaporanUserUseCase(
          isiForm: isiForm,
          onError: (e) {
            TampilanDialog.alertDialog("Gagal Proses", "${e}");
          },
          onNext: (theRespon) {
            TampilanDialog.showDialogPesanAlert(
                    "Berhasil", "Data sudah tersimpan",
                    the_icon: TemaIkonDialog.succcess)
                .then((value) {
              Get.back();
            });
          });
    }
  }

  @override
  void onInitState() {
    final planDate = DateTime.now();
    labelTanggal =
        DateFormat("EEEE, dd MMMM yyyy HH:mm", "id_ID").format(planDate);
    txtCtrlLabelTanggal.text =
        DateFormat("EEEE, dd MMMM yyyy HH:mm", "id_ID").format(planDate);
    Future.delayed(Duration(seconds: 1), () {
      stopLoading();
    });
    Future.delayed(Duration(seconds: 2), () {
      pilihKategori(selectedKategori);
    });
  }

  bool onLoading = true;
  void startLoading() {
    onLoading = true;
    notifyListeners();
  }

  void stopLoading() {
    onLoading = false;
    notifyListeners();
  }

  final formKey = GlobalKey<FormBuilderState>();

  @override
  void initListeners() {
    //use_case_initListener
//startPresenterListenerOnUseCaseInsertLaporanUserUseCase
    _form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnNext =
        (Respon the_value) {
      this.ListenUseCaseInsertLaporanUserUseCaseOnNext(the_value);
    };
    _form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnError =
        this.ListenUseCaseInsertLaporanUserUseCaseOnError;
    _form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnComplete =
        this.ListenUseCaseInsertLaporanUserUseCaseOnComplete;
//stopPresenterListenerOnUseCaseInsertLaporanUserUseCase
  }

  BuildContext? dialogContext;
  void handlePermissions() {}
  void dispose() => _form_laporan_presenter.dispose();

//auto_darttecture_class_body
//startControllerCallUseCaseInsertLaporanUserUseCase
  static defaultFuncONNextInsertLaporanUserUseCase(Respon theValue) {}
  static defaultFuncONErrorInsertLaporanUserUseCase(String errorMessage) {}
  static defaultFuncONCompleteInsertLaporanUserUseCase() {}
  void callInsertLaporanUserUseCase(
      {required IsiFormLaporan isiForm,
      Function(Respon) onNext = defaultFuncONNextInsertLaporanUserUseCase,
      Function(String errorMessageByUseCaseInsertLaporanUserUseCase) onError =
          defaultFuncONErrorInsertLaporanUserUseCase,
      Function onComplete =
          defaultFuncONCompleteInsertLaporanUserUseCase}) async {
    tambahan_callInsertLaporanUserUseCaseOnNext = onNext;
    tambahan_callInsertLaporanUserUseCaseOnError = onError;
    tambahan_callInsertLaporanUserUseCaseOnComplete = onComplete;
    StatusByUseCaseInsertLaporanUserUseCase = "ongoing";
    StartedUseCaseInsertLaporanUserUseCase = true;
    //showLoading();
    // so the animation can be seen
    print("controller try callInsertLaporanUserUseCase");
    Future.delayed(Duration(seconds: 0),
        () => _form_laporan_presenter.callInsertLaporanUserUseCase(isiForm));
  }

//endControllerCallUseCaseInsertLaporanUserUseCase
//startListenerOnUseCaseInsertLaporanUserUseCase
  void ListenUseCaseInsertLaporanUserUseCaseOnNext(Respon the_value) {
    //get called when usecase InsertLaporanUserUseCase return value
    //dismissLoading();
    StatusByUseCaseInsertLaporanUserUseCase = "onnext";
//startDefaultFunctionOnListenUseCaseInsertLaporanUserUseCaseOnNext
//endDefaultFunctionOnListenUseCaseInsertLaporanUserUseCaseOnNext
    if (tambahan_callInsertLaporanUserUseCaseOnNext != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callInsertLaporanUserUseCaseOnNext());
      tambahan_callInsertLaporanUserUseCaseOnNext(the_value);
    }
    //
    //print("dapat layanan : " + the_categories.length.toString());
    //refreshUI();
  }

  void ListenUseCaseInsertLaporanUserUseCaseOnError(e) {
    StatusByUseCaseInsertLaporanUserUseCase = "onerror";
    MessageByUseCaseInsertLaporanUserUseCase = e.toString();
    //get called when usecase InsertLaporanUserUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseInsertLaporanUserUseCaseOnError
//endDefaultFunctionOnListenUseCaseInsertLaporanUserUseCaseOnError
    if (tambahan_callInsertLaporanUserUseCaseOnError != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callInsertLaporanUserUseCaseonError());
      tambahan_callInsertLaporanUserUseCaseOnError(e);
    }
    //layanan_list = null;
    print("dapat error dari InsertLaporanUserUseCase");
    //refreshUI();
  }

  void ListenUseCaseInsertLaporanUserUseCaseOnComplete() {
    StatusByUseCaseInsertLaporanUserUseCase = "oncomplete";
    //get called when usecase InsertLaporanUserUseCase return error
    //dismissLoading();
    //showGenericSnackbar(getStateKey(), e.message, isError: true);
//startDefaultFunctionOnListenUseCaseInsertLaporanUserUseCaseOnComplete
//endDefaultFunctionOnListenUseCaseInsertLaporanUserUseCaseOnComplete
    if (tambahan_callInsertLaporanUserUseCaseOnComplete != null) {
//Future.delayed(Duration(seconds: 0), ()=>tambahan_callInsertLaporanUserUseCaseonComplete());
      tambahan_callInsertLaporanUserUseCaseOnComplete();
    }
    //layanan_list = null;
    print("dapat oncomplete dari InsertLaporanUserUseCase");
    //refreshUI();
  }
//stopListenerOnUseCaseInsertLaporanUserUseCase
}
//auto_darttecture_class_outside
