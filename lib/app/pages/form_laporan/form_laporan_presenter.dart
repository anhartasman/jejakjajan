import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:flutter/foundation.dart';
import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';

import 'package:jejakjajan/domain/repositories/UserRepository.dart';
import 'package:jejakjajan/domain/usecases/user/InsertLaporanUserUseCase.dart';

import 'package:jejakjajan/domain/usecases/user/InsertLaporanUserUseCase.dart';

class form_laporan_presenter extends Presenter {
  UserRepository _UserRepository;
  LaporanUserRepository _LaporanUserRepository;
  //auto_darttecture_class_var_declaration
//startPresenterUseCaseVarDeclarationForInsertLaporanUserUseCase
  late Function ListenUseCaseInsertLaporanUserUseCaseOnError;
  late Function ListenUseCaseInsertLaporanUserUseCaseOnNext;
  late Function ListenUseCaseInsertLaporanUserUseCaseOnComplete;
  InsertLaporanUserUseCase? _InsertLaporanUserUseCase;
//endPresenterUseCaseVarDeclarationForInsertLaporanUserUseCase
  form_laporan_presenter(
    this._LaporanUserRepository,
    this._UserRepository,
  ) {
    //auto_darttecture_usecase_initialize_repo
//start_usecase_initialize_repo_forInsertLaporanUserUseCase
    _InsertLaporanUserUseCase =
        InsertLaporanUserUseCase(_LaporanUserRepository, _UserRepository);
//end_usecase_initialize_repo_forInsertLaporanUserUseCase
  }
  void dispose() {
    //auto_darttecture_usecase_dispose
    _InsertLaporanUserUseCase?.dispose();
  }

//auto_darttecture_class_body
//startFunctionCallInsertLaporanUserUseCase
  void callInsertLaporanUserUseCase(IsiFormLaporan isiForm) {
    print("eksekusi InsertLaporanUserUseCase");
    _InsertLaporanUserUseCase?.execute(_InsertLaporanUserUseCaseObserver(this),
        InsertLaporanUserUseCaseParams(isiForm));
  }
//stopFunctionCallInsertLaporanUserUseCase

}

//auto_darttecture_class_outside
//startPresenterObserverUseCaseInsertLaporanUserUseCase
class _InsertLaporanUserUseCaseObserver implements Observer<Respon> {
  // The above presenter
  form_laporan_presenter _form_laporan_presenter;
  _InsertLaporanUserUseCaseObserver(this._form_laporan_presenter);

  /// implement if the `Stream` emits a value
  void onNext(the_value) {
    if (_form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnNext !=
        null) {
      _form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnNext(
          the_value);
    }
  }

  /// Login is successfull, trigger event in [LoginController]
  void onComplete() {
    // any cleaning or preparation goes here
    if (_form_laporan_presenter
            .ListenUseCaseInsertLaporanUserUseCaseOnComplete !=
        null) {
      _form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnComplete();
    }
  }

  /// Login was unsuccessful, trigger event in [LoginController]
  void onError(e) {
    // any cleaning or preparation goes here
    if (_form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnError !=
        null) {
      _form_laporan_presenter.ListenUseCaseInsertLaporanUserUseCaseOnError(e);
    }
  }
}
//endPresenterObserverUseCaseInsertLaporanUserUseCase
