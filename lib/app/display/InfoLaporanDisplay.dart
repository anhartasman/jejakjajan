import 'dart:convert';

import 'package:intl/intl.dart';

import 'package:flutter/foundation.dart';
import 'package:jejakjajan/app/utils/constants.dart';
import 'package:jejakjajan/domain/entities/KategoriLaporan.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';

class InfoLaporanDisplay {
  LaporanUser theLaporanUser;
  var theKategori = ConstKategori.makanan;
  InfoLaporanDisplay(this.theLaporanUser) {
    final dateTanggal =
        DateTime.fromMillisecondsSinceEpoch(theLaporanUser.tanggal);
    labelTanggal = DateFormat(formatJamTanggal, "id_ID").format(dateTanggal);
    final cariKategori = ConstKategori.cariById(theLaporanUser.kategori);
    if (cariKategori.success) {
      theKategori = cariKategori.the_respon!;
    }
  }
  String labelTanggal = "";
}
