import 'package:jejakjajan/domain/entities/KategoriLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/ResponGlobal.dart';

const formatTanggal = "EEEE, dd MMMM yyyy";
const formatJamTanggal = "EEEE, dd MMMM yyyy HH:mm";

class ConstKategori {
  static const KategoriLaporan makanan = const KategoriLaporan(
    id: 1,
    nama: "Makanan",
    kode: "makanan",
    icon_path: "assets/category_icon/uil_pizza-slice.png",
    icon_hex_color: "0xFFF2C94C",
  );
  static const KategoriLaporan internet = const KategoriLaporan(
    id: 2,
    nama: "Internet",
    kode: "internet",
    icon_path: "assets/category_icon/uil_rss-alt.png",
    icon_hex_color: "0xFF56CCF2",
  );
  static const KategoriLaporan edukasi = const KategoriLaporan(
    id: 3,
    nama: "Edukasi",
    kode: "edukasi",
    icon_path: "assets/category_icon/uil_book-open.png",
    icon_hex_color: "0xFFF2994A",
  );
  static const KategoriLaporan hadiah = const KategoriLaporan(
    id: 4,
    nama: "Hadiah",
    kode: "hadiah",
    icon_path: "assets/category_icon/uil_gift.png",
    icon_hex_color: "0xFFEB5757",
  );
  static const KategoriLaporan transport = const KategoriLaporan(
    id: 5,
    nama: "Transport",
    kode: "transport",
    icon_path: "assets/category_icon/uil_car-sideview.png",
    icon_hex_color: "0xFF9B51E0",
  );
  static const KategoriLaporan belanja = const KategoriLaporan(
    id: 6,
    nama: "Belanja",
    kode: "belanja",
    icon_path: "assets/category_icon/uil_shopping-cart.png",
    icon_hex_color: "0xFF27AE60",
  );
  static const KategoriLaporan alatRumah = const KategoriLaporan(
    id: 7,
    nama: "Alat Rumah",
    kode: "alatRumah",
    icon_path: "assets/category_icon/uil_home.png",
    icon_hex_color: "0xFFBB6BD9",
  );
  static const KategoriLaporan olahraga = const KategoriLaporan(
    id: 8,
    nama: "Olahraga",
    kode: "olahraga",
    icon_path: "assets/category_icon/uil_basketball.png",
    icon_hex_color: "0xFF2D9CDB",
  );
  static const KategoriLaporan hiburan = const KategoriLaporan(
    id: 9,
    nama: "Hiburan",
    kode: "hiburan",
    icon_path: "assets/category_icon/uil_clapper-board.png",
    icon_hex_color: "0xFF2F80ED",
  );
  static const List<KategoriLaporan> All = [
    makanan,
    internet,
    edukasi,
    hadiah,
    transport,
    belanja,
    alatRumah,
    olahraga,
    hiburan,
  ];
  static ResponGlobal<KategoriLaporan> cariById(int id) {
    var theRespon = ResponGlobal<KategoriLaporan>(success: false);
    for (var theKategori in All) {
      if (theKategori.id == id) {
        theRespon.success = true;
        theRespon.the_respon = theKategori;
        break;
      }
    }
    return theRespon;
  }
}
