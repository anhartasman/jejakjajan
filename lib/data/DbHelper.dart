import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/ResponGlobal.dart';
import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart' as sqlite;
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart' as path;

import 'database/queries/LaporanUserQuery.dart';
import 'database/queries/UserQuery.dart';

class DbHelper {
  //membuat method singleton
  static DbHelper _dbHelper = DbHelper._singleton();

  factory DbHelper() {
    return _dbHelper;
  }

  DbHelper._singleton();

  //baris terakhir singleton

  final tables = [
    UserQuery.CREATE_TABLE,
    LaporanUserQuery.CREATE_TABLE
  ]; // membuat daftar table yang akan dibuat

  Future<Database> openDB() async {
    final dbPath = await sqlite.getDatabasesPath();
    return sqlite.openDatabase(path.join(dbPath, 'thengoding.db'),
        onUpgrade: (db, oldVersion, newVersion) {
      tables.forEach((table) async {
        await db.execute("DROP TABLE ${table}");
        await db.execute(table).then((value) {
          print("berashil ");
        }).catchError((err) {
          print("errornya ${err.toString()}");
        });
      });
    }, onCreate: (db, version) {
      tables.forEach((table) async {
        await db.execute(table).then((value) {
          print("berashil ");
        }).catchError((err) {
          print("errornya ${err.toString()}");
        });
      });
      print('Table Created');
    }, version: 3);
  }

  Future<Respon> insertLaporan(
      UserAplikasi the_user, IsiFormLaporan isiForm) async {
    final db = await openDB();

    var theRespon = new Respon();
    var mapForm = isiForm.toMap();
    mapForm["userId"] = the_user.id;
    try {
      await db.insert(LaporanUserQuery.TABLE_NAME, mapForm,
          conflictAlgorithm: ConflictAlgorithm.replace);
    } catch (error) {
      theRespon.success = false;
      theRespon.error_msg = error.toString();
      theRespon.error_code = "001";
    }

    return theRespon;
  }

  Future<ResponGlobal<UserAplikasi>> login(
      String email, String password) async {
    final db = await openDB();
    var theRespon = new ResponGlobal<UserAplikasi>();
    try {
      String whereString = 'email = "${email}" AND password = "${password}"';

      List<Map<String, Object?>> result =
          await db.query(UserQuery.TABLE_NAME, where: whereString);

      if (result.length > 0) {
        var theOne = Map.of(result[0]);
        theOne.update("id", (value) => theOne["id"].toString());

        theRespon.the_respon = UserAplikasi.fromMap(theOne);

        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setString("currentAccount", theRespon.the_respon!.toJson());
      } else {
        throw ("Email ${email} not found");
      }
    } catch (error) {
      theRespon.success = false;
      theRespon.error_msg = error.toString();
    }

    return theRespon;
  }

  Future<ResponGlobal<UserAplikasi>> register(
      String name, String email, String password) async {
    final db = await openDB();
    var theRespon = new ResponGlobal<UserAplikasi>();
    try {
      String whereString = 'email = "${email}"';

      List<Map<String, Object?>> result =
          await db.query(UserQuery.TABLE_NAME, where: whereString);

      if (result.length > 0) {
        throw ("Email ${email} already exist");
      } else {
        await db.insert(UserQuery.TABLE_NAME, {
          "name": name,
          "email": email,
          "password": password,
        }).catchError((e) {
          throw (e.toString());
        });

        theRespon.the_respon =
            UserAplikasi(id: "", name: name, email: email, password: password);
      }
    } catch (error) {
      print("Gagal register " + error.toString());
      theRespon.success = false;
      theRespon.error_msg = error.toString();
    }

    return theRespon;
  }

  Future<List<LaporanUser>> getListLaporan(
      UserAplikasi the_user, FilterLaporan theFilter) async {
    final db = await openDB();
    List<LaporanUser> theRespon = [];
    try {
      String whereAbsenString =
          'tanggal >= ${theFilter.dateFrom} AND tanggal <= ${theFilter.dateTo}';

      List<Map<String, Object?>> result = [];
      if (theFilter.all) {
        result = await db.query(LaporanUserQuery.TABLE_NAME);
      } else {
        result = await db.query(LaporanUserQuery.TABLE_NAME,
            where: whereAbsenString);
      }
      for (var theResult in result) {
        var newMap = Map.of(theResult);
        newMap["id"] = newMap["id"].toString();

        var rowAbsen = LaporanUser.fromMap(newMap);

        theRespon.add(rowAbsen);
      }
    } catch (error) {
      print("Gagal getListAbsensi " + error.toString());
    }

    return theRespon;
  }

  insert(String table, Map<String, Object> data) {
    openDB().then((db) {
      db.insert(table, data, conflictAlgorithm: ConflictAlgorithm.replace);
    }).catchError((err) {
      print("error $err");
    });
  }

  Future<List> getData(String tableName) async {
    final db = await openDB();
    var result = await db.query(tableName);
    return result.toList();
  }
}
