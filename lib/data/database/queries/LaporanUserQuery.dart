class LaporanUserQuery {
  static const String TABLE_NAME = "LaporanUser";
  static const String CREATE_TABLE =
      " CREATE TABLE IF NOT EXISTS $TABLE_NAME ( id INTEGER PRIMARY KEY AUTOINCREMENT, userId TEXT, tanggal INTEGER, jumlah INTEGER, kategori INTEGER, nama TEXT ) ";
  static const String SELECT = "select * from $TABLE_NAME";
}
