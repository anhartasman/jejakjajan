import 'dart:async';
import 'dart:math';

import 'package:jejakjajan/data/database/queries/LaporanUserQuery.dart';
import 'package:jejakjajan/domain/entities/LaporanUser.dart';
import 'package:jejakjajan/domain/entities/FilterLaporan.dart';
import 'package:jejakjajan/domain/entities/IsiFormLaporan.dart';
import 'package:jejakjajan/domain/entities/Respon.dart';
import 'package:jejakjajan/domain/entities/ResponGlobal.dart';

import 'package:jejakjajan/domain/entities/UserAplikasi.dart';
import 'package:jejakjajan/domain/repositories/LaporanUserRepository.dart';

import '../DbHelper.dart';

/// `DataProdukRepository` is the implementation of `ProductCategoryRepository` present
/// in the Domain layer. It communicates with the server, making API calls to register, login, logout, and
/// store a `RumahPlong`.
class DataLaporanUserRepository implements LaporanUserRepository {
  Future<List<LaporanUser>> getLaporanUserList(
      UserAplikasi the_user, FilterLaporan theFilter) async {
    final DbHelper _helper = new DbHelper();
    theFilter.dateTo = theFilter.dateTo + (24 * 60 * 60 * 1000);
    List<LaporanUser> theRespon =
        await _helper.getListLaporan(the_user, theFilter);

    return Future.value(theRespon);
  }

  Future<Respon> insertData(
      UserAplikasi the_user, IsiFormLaporan isiForm) async {
    final DbHelper _helper = new DbHelper();
    final theRespon = _helper.insertLaporan(the_user, isiForm);
    return Future.value(theRespon);
  }
}
