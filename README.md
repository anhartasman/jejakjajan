# jejakjajan

Money Expense Application with Flutter and SQLite

## Jawaban Tes Pengetahuan

### Jawaban 1
Saya mulai menggunakan flutter sejak tahun 2019

### Jawaban 2
Library favorit saya adalah flutter_clean_architecture, berguna untuk menerapkan arsitektur clean architecture pada kodingan saya

### Jawaban 3
Saya biasa menggunakan metode Clean Architecture, yang memisahkan file untuk mengelola data dan menampilkan data

### Jawaban 4
Tantangan terbesar saya dalam menggunakan flutter adalah masih sedikitnya library yang beredar, maka terkadang saya harus membuat library sendiri

### Jawaban 5
Untuk keamanan data user, saya menggunakan firebase